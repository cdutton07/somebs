package frc.robot;

public class Constants {
    public class IntakeConst {
        public static final double kP = 0.0;
        public static final double kI = 0.0;
        public static final double kD = 0.0;
        public static final double ratio = 12.0/360.0;
    }
    public class ShooterConst {
        // Turret const
        public static final double ktP = 0.0;
        public static final double ktI = 0.0;
        public static final double ktD = 0.0;
        public static final double tRatio = 12.0/360.0;
        // Hood const
        public static final double khP = 0.0;
        public static final double khI = 0.0;
        public static final double khD = 0.0;
        public static final double hRatio = 12.0/360.0;

    }
}
