package frc.robot.subsystems;

import java.util.function.Supplier;

import com.ctre.phoenix6.configs.FeedbackConfigs;
import com.ctre.phoenix6.configs.Slot0Configs;
import com.ctre.phoenix6.controls.DutyCycleOut;
import com.ctre.phoenix6.controls.PositionDutyCycle;
import com.ctre.phoenix6.hardware.TalonFX;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.ShooterConst;
import frc.robot.RobotMap.Hardware;

public class Shooter extends SubsystemBase {
    private final TalonFX turret;
    private final TalonFX hood;
    private final TalonFX flywheel;
    private final TalonFX firing;

    private final DutyCycleOut flywheelOut;
    private final DutyCycleOut firingOut;

    private final PositionDutyCycle turretOut;
    private final PositionDutyCycle hoodOut;
    
    private final Supplier<Double> in;

    public Shooter(Supplier<Double> in) {
        turret = new TalonFX(Hardware.INTAKE_ONE);
        hood = new TalonFX(Hardware.INTAKE_EXT_ONE);
        flywheel = new TalonFX(Hardware.INTAKE_TWO);
        firing = new TalonFX(Hardware.INTAKE_EXT_TWO);
        
        Slot0Configs tSlot0Configs = new Slot0Configs();
        tSlot0Configs.kP = ShooterConst.ktP; 
        tSlot0Configs.kI = ShooterConst.ktI;
        tSlot0Configs.kD = ShooterConst.ktD;
        Slot0Configs hSlot0Configs = new Slot0Configs();
        hSlot0Configs.kP = ShooterConst.khP;
        hSlot0Configs.kI = ShooterConst.khI;
        hSlot0Configs.kD = ShooterConst.khD;

        FeedbackConfigs tFeedbackConfigs = new FeedbackConfigs();
        tFeedbackConfigs.SensorToMechanismRatio = ShooterConst.tRatio;
        FeedbackConfigs hFeedbackconfigs = new FeedbackConfigs();
        hFeedbackconfigs.SensorToMechanismRatio = ShooterConst.hRatio;

        turret.getConfigurator().apply(tSlot0Configs);
        hood.getConfigurator().apply(hSlot0Configs);
        turret.getConfigurator().apply(tFeedbackConfigs);
        hood.getConfigurator().apply(hFeedbackconfigs);

        flywheelOut = new DutyCycleOut(0);
        firingOut = new DutyCycleOut(0);
        turretOut = new PositionDutyCycle(0).withSlot(0);
        hoodOut = new PositionDutyCycle(0).withSlot(0);
        
        this.in = in;
    }

    public void fireOn() {
        firing.setControl(firingOut.withOutput(1));
    }
    
    public void fireOff() {
        firing.stopMotor();
    }

    public void runFlywheel(double speed) {
        flywheel.setControl(flywheelOut.withOutput(speed));
    }
    
    public void stopFlywheel() {
        flywheel.stopMotor();
    }

    public void rotate(double pos) {
        turret.setControl(turretOut.withPosition(pos));
    }

    public void rotateHood(double pos) {
        hood.setControl(hoodOut.withPosition(pos));
    }

    public boolean isHoodGood() {
        return hood.getClosedLoopError().getValue() < 1.0;
    }

    public void reset() {
        turret.setRotorPosition(0);
        hood.setRotorPosition(0);
    }

    @Override
    public void periodic() {
        rotate(in.get()*180.0);
    }
}
