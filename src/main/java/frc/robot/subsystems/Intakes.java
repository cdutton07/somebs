package frc.robot.subsystems;

import com.ctre.phoenix6.configs.FeedbackConfigs;
import com.ctre.phoenix6.configs.Slot0Configs;
import com.ctre.phoenix6.controls.DutyCycleOut;
import com.ctre.phoenix6.controls.PositionDutyCycle;
import com.ctre.phoenix6.hardware.TalonFX;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.IntakeConst;
import frc.robot.RobotMap.Hardware;

public class Intakes extends SubsystemBase {
    private final TalonFX intake1;
    private final TalonFX extend1;
    private final TalonFX intake2;
    private final TalonFX extend2;
    private final TalonFX indexer;

    private final DutyCycleOut oneOut;
    private final DutyCycleOut twoOut;
    private final DutyCycleOut indOut;

    private final PositionDutyCycle oneExtOut;
    private final PositionDutyCycle twoExtOut;

    private boolean oneOn = false;
    private boolean twoOn = false;

    public Intakes() {
        intake1 = new TalonFX(Hardware.INTAKE_ONE);
        extend1 = new TalonFX(Hardware.INTAKE_EXT_ONE);
        intake2 = new TalonFX(Hardware.INTAKE_TWO);
        extend2 = new TalonFX(Hardware.INTAKE_EXT_TWO);
        indexer = new TalonFX(Hardware.INDEXER);
        
        Slot0Configs slot0Configs = new Slot0Configs();
        slot0Configs.kP = IntakeConst.kP; 
        slot0Configs.kI = IntakeConst.kI;
        slot0Configs.kD = IntakeConst.kD;

        FeedbackConfigs feedbackConfigs = new FeedbackConfigs();
        // 12rot/1rot*1rot/360degree = 12.0/360.0
        feedbackConfigs.SensorToMechanismRatio = IntakeConst.ratio;

        extend1.getConfigurator().apply(slot0Configs);
        extend2.getConfigurator().apply(slot0Configs);
        extend1.getConfigurator().apply(feedbackConfigs);
        extend2.getConfigurator().apply(feedbackConfigs);

        oneOut = new DutyCycleOut(0);
        twoOut = new DutyCycleOut(0);
        indOut = new DutyCycleOut(0);
        oneExtOut = new PositionDutyCycle(0).withSlot(0);
        twoExtOut = new PositionDutyCycle(0).withSlot(0);
    }

    public void extend(boolean one, boolean two) {
        if (one) {
            oneOn = true;
            intake1.setControl(oneExtOut.withPosition(0));
            intake1.setControl(oneOut.withOutput(0.5));
        }
        if (two) {
            twoOn = true;
            intake2.setControl(twoExtOut.withPosition(0));
            intake2.setControl(twoOut.withOutput(0.5));
        }
        indexCheck();
    }

    public void retract(boolean one, boolean two) {
        if (one) {
            oneOn = false;
            intake1.setControl(oneExtOut.withPosition(90));
            intake1.stopMotor();
        }
        if (two) {
            twoOn = false;
            intake2.setControl(twoExtOut.withPosition(90));
            intake2.stopMotor();
        }
        indexCheck();
    }

    public void indexCheck() {
        if (oneOn || twoOn) indexOn();
        else indexOff();
    }

    public void indexOn() {
        indexer.setControl(indOut.withOutput(1));
    }

    public void indexOff() {
        indexer.stopMotor();
    }
}
