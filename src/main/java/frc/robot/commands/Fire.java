package frc.robot.commands;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Shooter;

public class Fire extends CommandBase {
    private final Shooter shooter;

    private final Timer t;

    public Fire(Shooter shooter) {
        this.shooter = shooter;
        t = new Timer();
    }

    @Override
    public void initialize() {
        t.reset();
        t.start();
        shooter.fireOn();
    }

    @Override
    public boolean isFinished() {
        return t.get() > 0.5;
    }

    @Override
    public void end(boolean interuppted) {
        shooter.fireOff();
        shooter.rotateHood(0);
        shooter.stopFlywheel();
    }
    
}
