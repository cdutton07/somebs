package frc.robot.commands;

import java.util.function.Supplier;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Shooter;

public class PrepShot extends CommandBase {
    private final Shooter shooter;
    
    private final Supplier<Double> hood;

    public PrepShot(Shooter shooter, Supplier<Double> in) {
        this.shooter = shooter;
        hood = in;
    }

    @Override
    public void initialize() {
        shooter.runFlywheel(0.8);
        shooter.rotateHood(hood.get()*45);
    }

    @Override
    public boolean isFinished() {
        return shooter.isHoodGood();
    }
}
