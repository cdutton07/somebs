package frc.robot.commands;

import java.util.function.Supplier;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.subsystems.Shooter;

public class PrepAndFire extends SequentialCommandGroup{
    public PrepAndFire(Shooter shooter, Supplier<Double> in) {
        addCommands(
            new PrepShot(shooter, in),
            new Fire(shooter)
        );
    }
}
