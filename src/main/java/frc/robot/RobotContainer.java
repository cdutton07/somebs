// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.Commands;
import edu.wpi.first.wpilibj2.command.StartEndCommand;
import edu.wpi.first.wpilibj2.command.button.Trigger;
import frc.robot.commands.Fire;
import frc.robot.commands.PrepAndFire;
import frc.robot.commands.PrepShot;
import frc.robot.subsystems.Intakes;
import frc.robot.subsystems.Shooter;

public class RobotContainer {
  private final Intakes intakes;
  private final Shooter shooter;

  private final XboxController controller;

  public RobotContainer() {
    controller = new XboxController(0);

    intakes = new Intakes();
    shooter = new Shooter(() -> controller.getRightY());

    configureBindings();
  }

  private void configureBindings() {
    new Trigger(() -> controller.getRightBumper()).toggleOnTrue(new StartEndCommand(() -> intakes.extend(true, false), () -> intakes.retract(true, false)));
    new Trigger(() -> controller.getRightTriggerAxis()>0.8).toggleOnTrue(new StartEndCommand(() -> intakes.extend(false, true), () -> intakes.retract(false, true)));
    
    new Trigger(() -> controller.getAButton()).onTrue(new PrepAndFire(shooter, () -> controller.getLeftTriggerAxis()));
    new Trigger(() -> controller.getBButton()).onTrue(new PrepShot(shooter, () -> controller.getLeftTriggerAxis()).andThen(new Fire(shooter)));
    new Trigger(() -> controller.getXButton()).onTrue(Commands.sequence(new PrepShot(shooter, () -> controller.getLeftTriggerAxis()), new Fire(shooter)));
    
  }

  public Command getAutonomousCommand() {
    return Commands.print("No autonomous command configured");
  }
}
