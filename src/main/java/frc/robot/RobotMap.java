package frc.robot;

public class RobotMap {
    public class Hardware {
        public static final int INTAKE_ONE = 9;
        public static final int INTAKE_TWO = 10;
        public static final int INTAKE_EXT_ONE = 11;
        public static final int INTAKE_EXT_TWO = 12;
        public static final int INDEXER = 12;
    }
}
